@extends('layout.master')

@section('title')
  show data cast dengan id : {{$cast->id}}
@endsection

@section('content')
    <h4>{{$cast->nama}}</h4>
    <p>Umur : {{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>
@endsection