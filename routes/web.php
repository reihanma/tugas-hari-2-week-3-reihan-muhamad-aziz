<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
Route::get('/master',function(){
    return view('layout.master');
});

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::get('/table', function(){
    return view('table.table');
});

//CRUD CAST
Route::get('/cast','Cast_controller@index');
Route::get('/cast/create','Cast_controller@create');
Route::post('/cast','Cast_controller@store');
Route::get('/cast/{cast_id}','Cast_controller@show');
Route::get('/cast/{cast_id}/edit','Cast_controller@edit');
Route::put('/cast/{cast_id}','Cast_controller@update');
Route::delete('/cast/{cast_id}','Cast_controller@destroy');